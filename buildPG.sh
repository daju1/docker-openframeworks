
echo Compiling the project generator code from within the container...
docker run --rm -it \
    --volume=$(pwd)/of:/of \
    ybycode/openframeworks \
    /of/scripts/linux/compilePG.sh -j4

echo DONE!
