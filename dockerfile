FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

COPY ./of/scripts/ /tmp/scripts

RUN apt-get update && apt-get install -y wget apt-utils

RUN /tmp/scripts/linux/ubuntu/install_dependencies.sh -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /of/apps
